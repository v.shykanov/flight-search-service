<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\Route;

/**
 * Class RouteRepository.
 *
 * @package namespace App\Repositories;
 */
class RouteRepository extends BaseRepository implements RouteRepositoryInterface
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'airline.id',
        'departureAirport.iata_code',
        'departureAirport.name',
        'arrivalAirport.iata_code',
        'arrivalAirport.name',
        'departure_datetime',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model(): string
    {
        return Route::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
