<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RouteRepositoryInterface.
 *
 * @package namespace App\Repositories;
 */
interface RouteRepositoryInterface extends RepositoryInterface
{
    /**
     * Load relation with closure
     *
     * @param string $relation
     * @param \Closure $closure
     *
     * @return $this
     */
    public function whereHas($relation, $closure);
}
