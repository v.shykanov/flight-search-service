<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Airport
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airport query()
 * @mixin \Eloquent
 */
class Airport extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'continent',
        'latitude',
        'longitude',
        'iata_code',
        'icao_code',
        'iso_country',
        'iso_region',
        'name',
    ];
}
