<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('airline_id');
            $table->unsignedInteger('departure_airport_id');
            $table->unsignedInteger('arrival_airport_id');
            $table->dateTime('departure_dateTime');
            $table->dateTime('arrival_dateTime');
            $table->unsignedInteger('duration');
            $table->string('flightNumber');
            $table->timestamps();

            $table->index('airline_id');
            $table->index('departure_airport_id');
            $table->index('arrival_airport_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
}
