<?php

use Illuminate\Database\Seeder;
use App\Models\Route as RouteModel;
use App\Models\Airport;

class RoutesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(RouteModel::class)
            ->create([
                'departure_datetime' => now(),
                'departure_airport_id' => function () {
                    return factory(Airport::class)->create([
                        'iata_code' => 'KBP',
                    ])->id;
                },
                'arrival_airport_id' => function () {
                    return factory(Airport::class)->create([
                        'iata_code' => 'BUD',
                    ])->id;
                },
            ]);
    }
}
